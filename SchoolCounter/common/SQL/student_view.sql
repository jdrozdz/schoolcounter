﻿CREATE VIEW all_students AS
SELECT s.student_id AS id, p.name AS imie, p.sec_name AS drugie_imie, p.lastname AS nazwisko,
	p.pesel, p.sex, a.street AS ulica, a.house_number AS nr_domu, a.village AS miejscowosc,
	a.zipcode AS kod_pocztowy, a.postoffice AS poczta, a.commune AS gmina,
	a.district AS powiat, a.province AS wojewodztwo
FROM is_students AS s
  JOIN is_personal AS p ON s.personal_id = p.personal_id
  JOIN is_addresses AS a ON s.address_id = a.address_id
ORDER BY s.student_id ASC;