﻿START TRANSACTION;

CREATE TABLE is_groups (
	group_id SERIAL NOT NULL PRIMARY KEY,
	name varchar(20) NOT NULL UNIQUE,
	prent_id INT NOT NULL DEFAULT 0
);

CREATE TABLE is_users (
	user_id SERIAL NOT NULL PRIMARY KEY,
	username VARCHAR(30) NOT NULL UNIQUE,
	password VARCHAR(100) NOT NULL,
	"group" VARCHAR(20) NOT NULL DEFAULT 1 REFERENCES is_groups("name"),
	name VARCHAR(60) NOT NULL,
	lastname VARCHAR(60) NOT NULL,
	email VARCHAR(60) NOT NULL UNIQUE,
	logged BOOLEAN NOT NULL DEFAULT 'false',
	lastlog INT NOT NULL DEFAULT 0,
	add_date DATE NOT NULL DEFAULT NOW(),
	del_date DATE
);

CREATE TABLE is_sex_types (
	sex_id SERIAL NOT NULL,
	sex VARCHAR(1) NOT NULL,
	name VARCHAR(20) NOT NULL
);
ALTER TABLE is_sex_types ADD CONSTRAINT sex_id_pk PRIMARY KEY (sex_id,"sex");
ALTER TABLE is_sex_types ADD CONSTRAINT sex_id_unique UNIQUE (sex_id);
ALTER TABLE is_sex_types ADD CONSTRAINT sex_unique UNIQUE (sex);

--
-- SEX TYPES ROWS
--
INSERT INTO is_sex_types(sex, name) VALUES ('K', 'Kobieta'), ('M','Mężczyzna'), ('N','Nie określona');

CREATE TABLE is_addresses(
	address_id SERIAL NOT NULL PRIMARY KEY,
	street VARCHAR(60) NOT NULL,
	house_number SMALLINT NOT NULL,
	apartment SMALLINT,
	village VARCHAR(90) NOT NULL,
	zipcode VARCHAR(6) NOT NULL,
	postoffice VARCHAR(90) NOT NULL,
	commune VARCHAR(60) NOT NULL,
	district VARCHAR(60) NOT NULL,
	province VARCHAR(60) NOT NULL
);

CREATE TABLE is_contact_types(
	ctype_id SERIAL NOT NULL,
	"name" VARCHAR(20) NOT NULL
);
ALTER TABLE is_contact_types ADD CONSTRAINT ctype_id_pk PRIMARY KEY (ctype_id,"name");
ALTER TABLE is_contact_types ADD CONSTRAINT ctype_id_unique UNIQUE (ctype_id);
ALTER TABLE is_contact_types ADD CONSTRAINT ctype_name_unique UNIQUE ("name");

--
-- CONTACT TYPES VALUES
--
INSERT INTO is_contact_types(name) VALUES ('telefon domowy'),('tel kom.'),('tel. praca'),('e-mail');

CREATE TABLE is_personal (
	personal_id SERIAL NOT NULL PRIMARY KEY,
	name VARCHAR(60) NOT NULL,
	sec_name VARCHAR(60),
	lastname VARCHAR(60) NOT NULL,
	pesel SMALLINT NOT NULL UNIQUE,
	born_place VARCHAR(60) NOT NULL,
	sex VARCHAR(1) NOT NULL REFERENCES is_sex_types(sex)
);

CREATE TABLE is_departments (
	department_id SERIAL NOT NULL,
	department VARCHAR(5),
	description VARCHAR(100)
);
ALTER TABLE is_departments ADD CONSTRAINT is_departments_pk PRIMARY KEY (department_id, department);
ALTER TABLE is_departments ADD CONSTRAINT is_deparyments_id UNIQUE (department_id);
ALTER TABLE is_departments ADD CONSTRAINT is_deparyments_department UNIQUE (department);

CREATE TABLE is_students(
	student_id SERIAL NOT NULL PRIMARY KEY,
	personal_id INT REFERENCES is_personal(personal_id),
	address_id INT REFERENCES is_addresses(address_id),
	add_date DATE NOT NULL DEFAULT NOW(),
	del_date TIMESTAMP,
	deleted_by INT
);

CREATE TABLE is_contact(
	contact_id SERIAL NOT NULL PRIMARY KEY,
	student_id INT NOT NULL,
	ctype VARCHAR(20) NOT NULL,
	"value" VARCHAR(30) NOT NULL,
	person VARCHAR(30) NOT NULL DEFAULT 'Opiekun'
);
ALTER TABLE is_contact ADD CONSTRAINT contact_type_fk FOREIGN KEY ("ctype") REFERENCES is_contact_types("name") MATCH FULL;
ALTER TABLE is_contact ADD CONSTRAINT contact_student_id_fk FOREIGN KEY (student_id) REFERENCES is_students(student_id);

CREATE TABLE is_dictionary_actions (
	dict_id SERIAL NOT NULL PRIMARY KEY,
	level_no SMALLINT DEFAULT 99,
	action_tag CHAR(1),
	"space" CHAR(1),
	message TEXT NOT NULL
);

--
-- DICTIONARY VALUES
--
-- S - Students, U - Users, E - Egzam
INSERT INTO is_dictionary_actions(level_no, action_tag,"space",message) VALUES 
(99,'A','S','Uczeń %s %s został dodany do bazy'),
(1,'E','S','Edycja wiersza %i w tabeli %s. Sprawdź ich poprawność!'), 
(1,'D','S','Uczeń %s %s ID: %i został usunięty przez %s - UID: %i'),
(99,'A','U','Użytkownik %s został dodany'),
(1,'D','U','Użytkownik %s został usunięty przez %s, UID: %i'),
(5,'A','E', 'Dodanie wyniku %i pkt dla %s przez %s, UID %i');

CREATE TABLE is_students_recruitment(
	recrut_id SERIAL NOT NULL PRIMARY KEY,
	students_id INT REFERENCES is_students(student_id),
	"result" INT NOT NULL,
	add_date DATE DEFAULT NOW()
);

CREATE TABLE is_recruitment_config(
	recrut_config_id SERIAL NOT NULL PRIMARY KEY,
	treshold INT NOT NULL DEFAULT 54,
	add_date DATE DEFAULT NOW(),
	active BOOLEAN DEFAULT 'true'
);

CREATE TABLE is_province (
	province_id SERIAL NOT NULL,
	province_name VARCHAR(30) NOT NULL,
	province_code VARCHAR(10) NOT NULL
);
ALTER TABLE is_province ADD CONSTRAINT province_id_pk PRIMARY KEY (province_id,province_name);
ALTER TABLE is_province ADD CONSTRAINT province_id_unique UNIQUE (province_id);
ALTER TABLE is_province ADD CONSTRAINT province_name_unique UNIQUE (province_name);
ALTER TABLE is_province ADD CONSTRAINT province_code_unique UNIQUE (province_code);

CREATE TABLE is_community (
	community_id SERIAL NOT NULL,
	community VARCHAR(50) NOT NULL,
	community_code VARCHAR(10)
);
ALTER TABLE is_community ADD CONSTRAINT community_id_pk PRIMARY KEY (community_id,community);
ALTER TABLE is_community ADD CONSTRAINT community_id_unique UNIQUE (community_id);
ALTER TABLE is_community ADD CONSTRAINT community__comm_unique UNIQUE (community);

CREATE TABLE is_town (
	town_id SERIAL NOT NULL,
	town_name VARCHAR(60) NOT NULL,
	zip_code VARCHAR(7) NOT NULL,
	town_code VARCHAR(10) NOT NULL
);
ALTER TABLE is_town ADD CONSTRAINT town_id_pk PRIMARY KEY (town_id,town_name,town_code);
ALTER TABLE is_town ADD CONSTRAINT town_id_unique UNIQUE (town_id);
ALTER TABLE is_town ADD CONSTRAINT town_name_unique UNIQUE (town_name);
ALTER TABLE is_town ADD CONSTRAINT town_code_unique UNIQUE (town_code);
ALTER TABLE is_town ADD CONSTRAINT town_zip_code_unique UNIQUE (zip_code);

COMMIT TRANSACTION;