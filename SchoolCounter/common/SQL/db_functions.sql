﻿create function setActive(_uid int) returns int as
$$
begin
	if _uid > 0 then
		update is_users set logged='true', lastlog=(select extract(epoch from now())) where user_id=_uid;
		return 1;
	else
		return 0;
	end if;
end;
$$
language plpgsql;

create function login(_username varchar,_passwd varchar)
returns int as
$$
declare
	_row RECORD;
begin
	SELECT * INTO _row FROM is_users WHERE username=_username AND password=md5(_passwd);
	if FOUND then
		perform setActive(_row.user_id);
		return _row.user_id;
	else
		return 0;
	end if;
end;
$$
language plpgsql;

create function logout(uid int) returns int as
$$
begin
	if uid > 0 then
		update is_users set lastlog=0, logged='false' where user_id=uid;
		return 1;
	else
		return 0;
	end if;
end;
$$
language plpgsql;

create function edit_student(_student_id int, _table varchar, _column varchar, _value varchar)
returns boolean as
$$
declare
  _row record;
  _tbl record;
begin
  select * into _row from is_students where student_id = _student_id;
  if found then
    select column_name as colName into _tbl from 
      information_schema.columns where table_name=_table limit 1 offset 0;
    if found then
      begin transaction
	update _table set _column=_value where _row.colName=_tbl.colName;
      commit transaction;
  else
    return false;
  end if;
end;
$$
