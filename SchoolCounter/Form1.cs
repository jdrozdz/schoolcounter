﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolCounter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.toolStripStatusLabel1.Text = Program.User.Name + " " + Program.User.Lastname;
        }

        private void newStudent_Click(object sender, EventArgs e)
        {
            NewStudent studentForm = new NewStudent();
            studentForm.Visible = true;
        }

        private void departmentsList_Click(object sender, EventArgs e)
        {
            Departments department = new Departments();
            department.Visible = true;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Authorization auth = new Authorization();
            auth.Visible = true;
        }

        private void exitProgram_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Czy na pewno chcesz zakończyć pracę?", "Koniec", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                SQLCommands sql = new SQLCommands();
                Application.Exit();
            }
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void bazaDanychToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseConfiguration dbc = new DatabaseConfiguration();
            dbc.Show();
        }
    }
}
