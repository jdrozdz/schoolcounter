﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Npgsql;

namespace SchoolCounter
{
    public partial class NewStudent : Form
    {
        SQLCommands sql = new SQLCommands();
        Boolean pesel = false;

        public NewStudent()
        {
            InitializeComponent();
            NpgsqlDataAdapter da = sql.getDepartments();
            NpgsqlCommand cmd = da.SelectCommand;

            Dictionary<int, String> _departments = new Dictionary<int, string>();
            NpgsqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    _departments.Add(rdr.GetInt32(0), rdr[1].ToString());
                }
                this._departmentList.DataSource = new BindingSource(_departments, null);
                this._departmentList.DisplayMember = "Value";
                this._departmentList.ValueMember = "Key";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _pesel_Leave(object sender, EventArgs e)
        {
            if (_pesel.Text.Length > 0 && _pesel.Text.Length == 11)
            {
                Validators validate = new Validators();
                validate.PESEL = Convert.ToInt64(_pesel.Text);
                if (validate.Pesel())
                {
                    _pesel.BackColor = Color.GreenYellow;
                    this.pesel = false;
                }
                else
                {
                    _pesel.BackColor = Color.Red;
                }
            }
            else
            {
                _pesel.BackColor = Color.Red;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pesel)
            {
                Program.Student.Name = this._name.Text;
                Program.Student.SName = this._sname.Text;
                Program.Student.Lastname = this._lastname.Text;
                Program.Student.Bournplace = this._bornplace.Text;
                Program.Student.Address = this._address.Text;
                Program.Student.Vilage = this._city.Text;
                Program.Student.Postoffice = this._postoffice.Text;
                Program.Student.ZipCode = this._zipcode.Text;
                Program.Student.PESEL = Convert.ToInt32(this._pesel.Text);

                if (sql.addStudent())
                {

                }
            }
        }
    }
}
