﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace SchoolCounter
{
    class Validators
    {
        private Int64 pesel;
        private String email;

        public Boolean Pesel()
        {
            int[] weight = { 9,7,3,1 };
            int Sum = 0;
            int checkSum = 0;

            int[] p = PESEL.ToString().ToCharArray().Select(x => (int)Char.GetNumericValue(x)).ToArray();

            int y;
            for (int i = 0; i <= 9; i++)
            {
                y = (p[i] * weight[i % 4]);
                Sum = (Sum + y);
            }
            checkSum = (10 - (Sum % 10));
            if (checkSum == p[10] || checkSum == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean Email()
        {
            try
            {
                MailAddress mail = new MailAddress(EMAIL);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public Int64 PESEL
        {
            get { return this.pesel; }
            set { this.pesel = value; }
        }

        public String EMAIL
        {
            get { return this.email; }
            set { this.email = value; }
        }
    }
}
