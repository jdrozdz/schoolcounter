﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolCounter
{
    [System.Serializable()]
    class DatabaseStruct
    {
        private String db_host;
        private int db_port;
        private String db_username;
        private String db_password;
        private String db_name;
        private String connString;


        public String Host
        {
            get { return db_host; }
            set { db_host = value; }
        }

        public int Port
        {
            get { return db_port; }
            set { db_port = value; }
        }

        public String Username
        {
            get { return db_username; }
            set { db_username = value; }
        }

        public String Password
        {
            get { return db_password; }
            set { db_password = value; }
        }

        public String DatabaseName
        {
            get { return db_name; }
            set { db_name = value; }
        }
    }
}
