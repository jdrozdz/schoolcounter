﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolCounter
{
    [System.Serializable]
    class UserStruct
    {
        private int _uid = 0;
        private String _username;
        private String _email;
        private String _name;
        private String _lastname;
        private Boolean _logged = false;
        private int _lastlog = 0;

        public int UID
        {
            get { return this._uid; }
            set { this._uid = value; }
        }

        public String Username
        {
            get { return this._username; }
            set { this._username = value; }
        }

        public String Name {
            get { return this._name;}
            set { this._name = value; }
        }

        public String Lastname
        {
            get { return this._lastname; }
            set { this._lastname = value; }
        }

        public String Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        public Boolean Logged
        {
            get { return this._logged; }
            set { this._logged = value; }
        }

        public int Lastlog
        {
            get { return this._lastlog; }
            set { this._lastlog = value; }
        }
    }
}
