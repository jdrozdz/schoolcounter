﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolCounter
{
    [System.Serializable]
    class StudentsStruct
    {
        private String name;
        private String sname;
        private String lastname;
        private String bornplace;
        private Int32 pesel;

        private String address;
        private String vilage;
        private String zipcode;
        private String postoffice;
        private Dictionary<String, String> contact;

        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public String SName
        {
            get { return this.sname; }
            set { this.sname = value; }
        }

        public String Lastname
        {
            get { return this.lastname; }
            set { this.lastname = value; }
        }

        public String Bournplace
        {
            get { return this.bornplace; }
            set { this.bornplace = value; }
        }

        public Int32 PESEL
        {
            get { return this.pesel; }
            set { this.pesel = value; }
        }

        public String Address
        {
            get { return this.address; }
            set { this.address = value; }
        }

        public String Vilage
        {
            get { return this.vilage; }
            set { this.vilage = value; }
        }

        public String ZipCode
        {
            get { return this.zipcode; }
            set { this.zipcode = value; }
        }

        public String Postoffice
        {
            get { return this.postoffice; }
            set { this.postoffice = value; }
        }

        public String this[String key]
        {
            get { return this.contact[key]; }
            set { this.contact[key] = value; }
        }
    }
}
