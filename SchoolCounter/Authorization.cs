﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolCounter
{
    public partial class Authorization : Form
    {
        public Authorization()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SQLCommands sql = new SQLCommands();
            sql.checkUser(_username.Text, _password.Text);
            if ( Program.User.UID > 0 )
            {
                Form1 baseForm = new Form1();
                this.Hide();
                baseForm.Show();
            }
            else
            {
                MessageBox.Show("Nie masz odpowiednich uprawnień do części programu!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Authorization_Load(object sender, EventArgs e)
        {
            this.AcceptButton = button1;
            this.CancelButton = button2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            DatabaseConfiguration dbcfg = new DatabaseConfiguration();
            dbcfg.Show();
            /*
            ConfigurationManager.Manager mgr = new ConfigurationManager.Manager();
            Dictionary<String, String> cfg = mgr.loadConfiguration();
            MessageBox.Show(cfg["username"]);*/
        }
    }
}
