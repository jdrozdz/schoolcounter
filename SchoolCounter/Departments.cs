﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolCounter
{
    public partial class Departments : Form
    {
        private int id = 1;
        SQLCommands sql = new SQLCommands();

        public Departments()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1.Rows[i].Selected = false;
            }
                dataGridView1.Rows[e.RowIndex].Selected = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String department = textBox1.Text;
            String shortName = textBox2.Text;

            if (department.Length > 0)
            {
                int rowIndex = -1;
                if (dataGridView1.RowCount > 0)
                {
                    DataGridViewRow row = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Where(r => r.Cells["id"].Value.ToString().Equals(department) || r.Cells["oddzial"].Value.ToString().Equals(shortName))
                        .FirstOrDefault();
                    if (row != null && (int)row.Index >= 0)
                    {
                        rowIndex = row.Index;
                    }
                }
                if (rowIndex < 0)
                {
                    Int32 x = this.sql.addDepartment(shortName, department);
                    this.GetData();
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show("Taki odddział już istnieje", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Nie podano nazwy oddziału", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Departments_Load(object sender, EventArgs e)
        {
            this.GetData();
        }

        private void GetData()
        {
            Npgsql.NpgsqlDataAdapter da = this.sql.getDepartments();
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.Columns[0].Width = 30;
            dataGridView1.Columns[2].Width = 250;
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
            if (e.ColumnIndex == 0)
            {
                MessageBox.Show("ID oddziału jest polem tylko do odczytu!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dataGridView1.Rows[e.RowIndex].Cells[0].Style.BackColor = Color.Red;
            }
            if (e.ColumnIndex == 1)
            {
                int x = this.sql.updateDepartment(id, "department", dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
                if (x == 0)
                {
                    MessageBox.Show("Nie można zaktualizwoać wpisu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.GreenYellow;
                }
            }
            if (e.ColumnIndex == 2)
            {
                int x = this.sql.updateDepartment(id, "description", dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
                if (x == 0)
                {
                    MessageBox.Show("Nie można zaktualizwoać wpisu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[2].Style.BackColor = Color.GreenYellow;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
