﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfigurationManager;

namespace SchoolCounter
{
    public partial class DatabaseConfiguration : Form
    {
        public DatabaseConfiguration()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Manager manager = new Manager();
            bool x = manager.saveFile(_configName.Text, _username.Text, _password.Text, _server.Text, Convert.ToInt16(_port.Text), _database.Text);
            if (x)
            {
                DialogResult b = MessageBox.Show("Konfiguracja została zapisana\nZamknąć okno konfiguracji?", "Informacja", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (b == DialogResult.Yes)
                {
                    this.Hide();
                }
            }
        }

        private void DatabaseConfiguration_Load(object sender, EventArgs e)
        {
            this.AcceptButton = button1;
            this.CancelButton = button2;
        }
    }
}
