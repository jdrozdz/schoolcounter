﻿namespace SchoolCounter
{
    partial class NewStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._name = new System.Windows.Forms.TextBox();
            this._lastname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._bornplace = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._address = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._city = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._zipcode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._postoffice = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._community = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._district = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this._province = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._pesel = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this._sname = new System.Windows.Forms.TextBox();
            this._departmentList = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _name
            // 
            this._name.Location = new System.Drawing.Point(47, 36);
            this._name.Name = "_name";
            this._name.Size = new System.Drawing.Size(234, 20);
            this._name.TabIndex = 0;
            // 
            // _lastname
            // 
            this._lastname.Location = new System.Drawing.Point(606, 36);
            this._lastname.Name = "_lastname";
            this._lastname.Size = new System.Drawing.Size(187, 20);
            this._lastname.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Imię:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(544, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nazwisko:";
            // 
            // _bornplace
            // 
            this._bornplace.FormattingEnabled = true;
            this._bornplace.Location = new System.Drawing.Point(113, 82);
            this._bornplace.Name = "_bornplace";
            this._bornplace.Size = new System.Drawing.Size(204, 21);
            this._bornplace.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Miejsce urodzenia:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Adres:";
            // 
            // _address
            // 
            this._address.Location = new System.Drawing.Point(55, 149);
            this._address.Name = "_address";
            this._address.Size = new System.Drawing.Size(226, 20);
            this._address.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Miejscowość:";
            // 
            // _city
            // 
            this._city.FormattingEnabled = true;
            this._city.Location = new System.Drawing.Point(364, 148);
            this._city.Name = "_city";
            this._city.Size = new System.Drawing.Size(229, 21);
            this._city.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(610, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Kod pocztowy:";
            // 
            // _zipcode
            // 
            this._zipcode.Location = new System.Drawing.Point(693, 148);
            this._zipcode.Name = "_zipcode";
            this._zipcode.Size = new System.Drawing.Size(100, 20);
            this._zipcode.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Poczta:";
            // 
            // _postoffice
            // 
            this._postoffice.FormattingEnabled = true;
            this._postoffice.Location = new System.Drawing.Point(61, 192);
            this._postoffice.Name = "_postoffice";
            this._postoffice.Size = new System.Drawing.Size(220, 21);
            this._postoffice.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(287, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Gmina:";
            // 
            // _community
            // 
            this._community.FormattingEnabled = true;
            this._community.Location = new System.Drawing.Point(333, 192);
            this._community.Name = "_community";
            this._community.Size = new System.Drawing.Size(260, 21);
            this._community.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Powiat:";
            // 
            // _district
            // 
            this._district.FormattingEnabled = true;
            this._district.Location = new System.Drawing.Point(61, 227);
            this._district.Name = "_district";
            this._district.Size = new System.Drawing.Size(220, 21);
            this._district.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(287, 230);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Województwo:";
            // 
            // _province
            // 
            this._province.FormattingEnabled = true;
            this._province.Location = new System.Drawing.Point(370, 227);
            this._province.Name = "_province";
            this._province.Size = new System.Drawing.Size(223, 21);
            this._province.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(330, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "PESEL:";
            // 
            // _pesel
            // 
            this._pesel.Location = new System.Drawing.Point(380, 82);
            this._pesel.Name = "_pesel";
            this._pesel.Size = new System.Drawing.Size(213, 20);
            this._pesel.TabIndex = 4;
            this._pesel.Leave += new System.EventHandler(this._pesel_Leave);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(718, 227);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "&Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(632, 227);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "&Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(288, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Drugie imię:";
            // 
            // _sname
            // 
            this._sname.Location = new System.Drawing.Point(356, 36);
            this._sname.Name = "_sname";
            this._sname.Size = new System.Drawing.Size(182, 20);
            this._sname.TabIndex = 1;
            // 
            // _departmentList
            // 
            this._departmentList.FormattingEnabled = true;
            this._departmentList.Location = new System.Drawing.Point(659, 82);
            this._departmentList.Name = "_departmentList";
            this._departmentList.Size = new System.Drawing.Size(134, 21);
            this._departmentList.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(606, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Oddział:";
            // 
            // NewStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 267);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this._departmentList);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._pesel);
            this.Controls.Add(this.label12);
            this.Controls.Add(this._province);
            this.Controls.Add(this.label11);
            this.Controls.Add(this._district);
            this.Controls.Add(this.label10);
            this.Controls.Add(this._community);
            this.Controls.Add(this.label9);
            this.Controls.Add(this._postoffice);
            this.Controls.Add(this.label8);
            this.Controls.Add(this._zipcode);
            this.Controls.Add(this.label7);
            this.Controls.Add(this._city);
            this.Controls.Add(this.label6);
            this.Controls.Add(this._address);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._bornplace);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._lastname);
            this.Controls.Add(this._sname);
            this.Controls.Add(this._name);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(833, 305);
            this.Name = "NewStudent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nowy uczeń";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _name;
        private System.Windows.Forms.TextBox _lastname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _bornplace;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _address;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox _city;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox _zipcode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _postoffice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _community;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _district;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox _province;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _pesel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _sname;
        private System.Windows.Forms.ComboBox _departmentList;
        private System.Windows.Forms.Label label3;
    }
}