﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Security.Cryptography;
/**
 * REMEMBER!!!
 * Encryptwd data: username, password, host, db name
 * 
 */ 
namespace ConfigurationManager
{
    class Manager
    {

        public XmlDocument xml = new XmlDocument();
        protected String path = Directory.GetCurrentDirectory();

        /**
         * Cryptography settings
         */
        private const string initVector = "tu89geji340t89u2";
        private const String passPhrase = "AtarEEWqksdp712$@#1*^";
        private const int keysize = 256;
        //***-----------------------------------***

        public Manager() { }

        public Dictionary<String, String> loadConfiguration()
        {
            Dictionary<String, String> config = new Dictionary<String, String>();
            this.loadFile();
            XmlNodeList xnode = this.xml.GetElementsByTagName("Default");
            config["username"] = xnode.Item(0).Attributes["db_username"].Value;
            config["password"] = this.Decrypt(xnode.Item(0).Attributes["db_password"].Value);
            config["server"] = xnode.Item(0).Attributes["db_host"].Value;
            config["port"] = xnode.Item(0).Attributes["db_port"].Value;
            config["database"] = xnode.Item(0).Attributes["db_name"].Value;

            return config;
        }

        public void loadFile()
        {
            if (File.Exists(path + @"\config\database.xml"))
            {
                this.xml.Load(path + @"\config\database.xml");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Nie ma pliku konfiguracyjnego, proszę o stworzenie konfiguracji", "Informacja", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// Zapisywanie konfifuracji bazy
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <param name="_host"></param>
        /// <param name="_port"></param>
        /// <param name="_dbname"></param>
        /// <returns></returns>
        public Boolean saveFile(String nodeName, String _username, String _password, String _host, int _port, String _dbname)
        {
            if (!Directory.Exists(path + @"\config"))
            {
                Directory.CreateDirectory(path + @"\config");
            }
            if (!File.Exists(path + @"\config\database.xml"))
            {
                XmlNode doc_dec = this.xml.CreateXmlDeclaration("1.0", "UTF-8", "yes");
                this.xml.AppendChild(doc_dec);
                XmlNode root = this.xml.CreateElement("database");
                XmlNode node = this.setNewAttribs(nodeName, _username, _password, _host, _port, _dbname);

                root.AppendChild(node);
                this.xml.AppendChild(root);
            }
            else
            {
                this.loadFile();
                XmlNodeList node = this.xml.GetElementsByTagName(nodeName);
                if (node.Count == 1)
                {
                    node = this.updateAttribs(node, _username, _password, _host, _port, _dbname);
                }
                else
                {
                    XmlNode root = this.xml.DocumentElement;
                    XmlNode _node = this.setNewAttribs(nodeName, _username, _password, _host, _port, _dbname);
                    root.AppendChild(_node);
                    this.xml.AppendChild(root);
                }
            }
            try
            {
                this.xml.Save(path + @"\config\database.xml");
                return true;
                //System.Windows.Forms.MessageBox.Show("Konfiguracja została zapisana", "Zisywanie", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            catch (XmlException exc)
            {
                return false;
                //System.Windows.Forms.MessageBox.Show(exc.Message, "Zisywanie", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

        }

        private XmlNodeList updateAttribs(XmlNodeList _node, String _username, String _password, String _host, int _port, String _dbname)
        {
            _node.Item(0).Attributes["db_name"].Value = _dbname;
            _node.Item(0).Attributes["db_username"].Value = _username;
            _node.Item(0).Attributes["db_password"].Value = this.Encrypt(_password);
            _node.Item(0).Attributes["db_port"].Value = _port.ToString();
            _node.Item(0).Attributes["db_host"].Value = _host;

            return _node;
        }

        private XmlNode setNewAttribs(String nodeName, String _username, String _password, String _host, int _port, String _dbname)
        {
            XmlNode node = this.xml.CreateElement(nodeName);
            XmlAttribute dbusername = this.xml.CreateAttribute("db_username");
            XmlAttribute dbpassword = this.xml.CreateAttribute("db_password");
            XmlAttribute dbhost = this.xml.CreateAttribute("db_host");
            XmlAttribute dbport = this.xml.CreateAttribute("db_port");
            XmlAttribute dbname = this.xml.CreateAttribute("db_name");
            XmlAttribute name = this.xml.CreateAttribute("name");

            dbusername.Value = Encrypt(_username);
            dbpassword.Value = Encrypt(_password);
            dbhost.Value = Encrypt(_host);
            dbport.Value = _port.ToString();
            dbname.Value = Encrypt(_dbname);
            name.Value = nodeName;

            node.Attributes.Append(name);
            node.Attributes.Append(dbusername);
            node.Attributes.Append(dbpassword);
            node.Attributes.Append(dbhost);
            node.Attributes.Append(dbport);
            node.Attributes.Append(dbname);

            return node;
        }

        public String Encrypt(string plainText)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        public String Decrypt(string cipherText)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }
    }
}
