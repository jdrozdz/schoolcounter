﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigurationManager;
using Npgsql;
using NpgsqlTypes;
using System.Windows.Forms;

namespace SchoolCounter
{
    class SQLCommands
    {
        private String _connectionString = "Server={0}; Port={1}; User Id={2}; Password={3}; Database={4}";
        private String connString = null;
        private Manager configuration = new Manager();
        private NpgsqlConnection dbh;
        private NpgsqlCommand cmd;
        private NpgsqlDataReader rdr;
		private NpgsqlDataAdapter da;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SchoolCounter.SQLCommands"/> class.
		/// </summary>
        public SQLCommands()
        {
            Dictionary<String, String> cfg = configuration.loadConfiguration();
            this.connString = String.Format(this._connectionString, cfg["server"], cfg["port"], cfg["username"], cfg["password"], cfg["database"]);
            this.dbh = new NpgsqlConnection(this.connString);

            if (this.dbh.State != System.Data.ConnectionState.Open)
            {
                this.dbh.Open();
            }
        }
		
		/// <summary>
		/// Adds the department.
		/// </summary>
		/// <returns>
		/// The department.
		/// </returns>
		/// <param name='department'>
		/// Department.
		/// </param>
		/// <param name='desc'>
		/// Desc.
		/// </param>
        public Int32 addDepartment(String department, String desc)
        {
            String sql = String.Format("INSERT INTO is_departments(department, description) VALUES('{0}','{1}')", department, desc);
            this.cmd = new NpgsqlCommand(sql, this.dbh);
            return this.cmd.ExecuteNonQuery();
        }
		
		/// <summary>
		/// Adds the user.
		/// </summary>
		/// <returns>
		/// The user.
		/// </returns>
		/// <param name='_username'>
		/// _username.
		/// </param>
		/// <param name='_password'>
		/// _password.
		/// </param>
		/// <param name='_name'>
		/// _name.
		/// </param>
		/// <param name='_lastname'>
		/// _lastname.
		/// </param>
		/// <param name='_email'>
		/// _email.
		/// </param>
        public Int32 addUser(String _username, String _password, String _name, String _lastname, String _email)
        {
            String sql = String.Format(@"INSERT INTO is_users(username, password, name, lastname, email)
                                        VALUES ('{0}',md5('{1}'),'{2}','{3}','{4}')", _username, _password, _name, _lastname, _email);
            this.cmd = new NpgsqlCommand(sql, this.dbh);
            return this.cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Check the username and password are correct and the user exists
        /// </summary>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <returns>int user_id</returns>
        public void checkUser(String _username, String _password)
        {
            String sql = String.Format("select login('{0}','{1}') as uid;", _username, _password);
            this.cmd = new NpgsqlCommand(sql, this.dbh);
            try
            {
                this.rdr = this.cmd.ExecuteReader();
            }
            catch (NpgsqlException ext)
            {
                this.ShowError(ext);
            }
            if (this.rdr.HasRows)
            {
                this.rdr.Close();
                this.rdr = null;

                this.cmd = new NpgsqlCommand(String.Format("SELECT * FROM is_users WHERE username='{0}' AND password=md5('{1}') AND logged='true'",_username, _password), this.dbh);
                try
                {
                    this.rdr = this.cmd.ExecuteReader();
                }
                catch (NpgsqlException ext)
                {
                    this.ShowError(ext);
                }
                this.rdr.Read();
                if (this.rdr.HasRows)
                {
                    Program.User.UID = this.rdr.GetInt32(this.rdr.GetOrdinal("user_id"));
                    Program.User.Name = this.rdr.GetString(this.rdr.GetOrdinal("name"));
                    Program.User.Lastname = this.rdr.GetString(this.rdr.GetOrdinal("lastname"));
                    Program.User.Email = this.rdr.GetString(this.rdr.GetOrdinal("email"));
                    Program.User.Logged = this.rdr.GetBoolean(this.rdr.GetOrdinal("logged"));
                    Program.User.Lastlog = this.rdr.GetInt32(this.rdr.GetOrdinal("lastlog"));
                }
            }
        }
		
		/// <summary>
		/// Gets the departments.
		/// </summary>
		/// <returns>
		/// The departments.
		/// </returns>
        public NpgsqlDataAdapter getDepartments()
        {
            this.da = new NpgsqlDataAdapter("SELECT department_id AS ID, department AS Oddzial, description AS Nazwa FROM is_departments ORDER BY department_id ASC", this.dbh);
            return da;
        }
		
		/// <summary>
		/// Gets the students.
		/// </summary>
		/// <returns>
		/// The students.
		/// </returns>
		public NpgsqlDataAdapter getStudents()
		{
			this.da = new NpgsqlDataAdapter("SELECT * FROM all_students", this.dbh);
			return da;
		}

        public Boolean addStudent()
        {
            
            return true;
        }
		
		/// <summary>
		/// Updates the student.
		/// </summary>
		/// <returns>
		/// The student.
		/// </returns>
		/// <param name='_student_id'>
		/// _student_id.
		/// </param>
		/// <param name='_table'>
		/// _table.
		/// </param>
		/// <param name='_column'>
		/// _column.
		/// </param>
		/// <param name='_value'>
		/// _value.
		/// </param>
		public Boolean updateStudent(Int32 _student_id, String _table, String _column, String _value)
		{
			String sql = String.Format("SELECT edit_student({0}, '{1}', '{2}','{3}')",
			                           _student_id, _table, _column, _value);
			Int32 result = 0;
			this.cmd = new NpgsqlCommand(sql, this.dbh);
			try
			{
				result = this.cmd.ExecuteNonQuery();
			}
			catch(NpgsqlException ext)
			{
				this.ShowError(ext);
			}
			if(result > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// Updates the department.
		/// </summary>
		/// <returns>
		/// The department.
		/// </returns>
		/// <param name='dip'>
		/// Dip.
		/// </param>
		/// <param name='_column'>
		/// _column.
		/// </param>
		/// <param name='_value'>
		/// _value.
		/// </param>
        public Int32 updateDepartment(int dip, String _column, String _value)
        {
            String sql = String.Format("update is_departments set {0}='{1}' where department_id={2}", _column, _value, dip);
            Int32 result = 0;
            this.cmd = new NpgsqlCommand(sql, this.dbh);
            try
            {
                result = this.cmd.ExecuteNonQuery();
            }
            catch (NpgsqlException ext)
            {
                this.ShowError(ext);
            }
            return result;
        }
		
		/// <summary>
		/// Logout this instance.
		/// </summary>
        public void Logout()
        {
            String sql = String.Format("select logout("+Program.User.UID+")");
			this.cmd = new NpgsqlCommand(sql, this.dbh);
			
			try
			{
				this.cmd.ExecuteNonQuery();
			}
			catch(NpgsqlException ext)
			{
				this.ShowError(ext);
			}
        }
		
		/// <summary>
		/// Opens the connection.
		/// </summary>
        public void OpenConnection() {
            if (this.dbh.State != System.Data.ConnectionState.Open)
            {
                this.dbh.Open();
            }
            else
            {
                MessageBox.Show("Połączenie z bazą danych jest otwarte!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
		
		/// <summary>
		/// Closes the connection.
		/// </summary>
        public void CloseConnection()
        {
            if (this.dbh.State != System.Data.ConnectionState.Closed)
            {
                this.dbh.Close();
            }
            else
            {
                MessageBox.Show("Połączenie z bazą danych jest zamknięte", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
		
		/// <summary>
		/// Shows the error.
		/// </summary>
		/// <param name='ext'>
		/// Ext.
		/// </param>
        private void ShowError(NpgsqlException ext)
        {
            MessageBox.Show(ext.BaseMessage + "\n" + ext.Detail, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
