﻿CREATE TABLE personal_data(
	person_id SERIAL NOT NULL,
	name VARCHAR(50) NOT NULL,
	name1 VARCHAR(50),
	lastname VARCHAR(60) NOT NULL,
	birthday date NOT NULL,
	pesel INT NOT NULL,
	town_born VARCHAR(100) NOT NULL,
	fader_name VARCHAR(50) NOT NULL,
	mother_name VARCHAR(50) NOT NULL
);
ALTER TABLE personal_data ADD CONSTRAINT personal_data_pk PRIMARY KEY (person_id, pesel);
ALTER TABLE personal_data ADD CONSTRAINT person_id_uinique UNIQUE (person_id, pesel);
/*
CREATE TABLE contact_type();
CREATE TYPE contact_data();
*/
CREATE TABLE province (
	province_id SERIAL NOT NULL,
	province_name VARCHAR(30) NOT NULL,
	province_code VARCHAR(10) NOT NULL
);
ALTER TABLE province ADD CONSTRAINT province_pk PRIMARY KEY (province_id, province_name);
ALTER TABLE province ADD CONSTRAINT province_unique UNIQUE (province_id, province_name, province_code);

CREATE TABLE community (
	community_id SERIAL NOT NULL,
	community VARCHAR(50) NOT NULL,
	community_code VARCHAR(10)
);
ALTER TABLE community ADD CONSTRAINT community_pk PRIMARY KEY (community_id, community);
ALTER TABLE community ADD CONSTRAINT community_unique UNIQUE (community_id, community);

CREATE TABLE town (
	town_id SERIAL NOT NULL,
	town_name VARCHAR(60) NOT NULL,
	zip_code VARCHAR(7) NOT NULL,
	town_code VARCHAR(10) NOT NULL
);
ALTER TABLE town ADD CONSTRAINT town_pk PRIMARY KEY (town_id, town_name, town_code);
ALTER TABLE town ADD CONSTRAINT town_unique UNIQUE (town_id, town_name, town_code, zip_code);

CREATE TABLE address (
	address_id SERIAL NOT NULL,
	street VARCHAR(160) NOT NULL,
	town VARCHAR(60) NOT NULL,
	zip_code VARCHAR(7) NOT NULL,
	province VARCHAR(30) NOT NULL,
	community VARCHAR(50) NOT NULL,
	is_default BOOLEAN NOT NULL default 'false'
);
ALTER TABLE address ADD CONSTRAINT address_pk PRIMARY KEY (address_id);
ALTER TABLE address ADD CONSTRAINT address_unique UNIQUE (address_id);
ALTER TABLE address ADD CONSTRAINT address_town_fk FOREIGN KEY (town) REFERENCES town(town_name) MATCH FULL;
ALTER TABLE address ADD CONSTRAINT address_town_fk FOREIGN KEY (zip_code) REFERENCES town(zip_code) MATCH FULL;
ALTER TABLE address ADD CONSTRAINT address_province FOREIGN KEY (province) REFERENCES province(province_name) MATCH FULL;
ALTER TABLE address ADD CONSTRAINT address_community FOREIGN KEY (community) REFERENCES community(community) MATCH FULL;

CREATE TABLE classes (
	class_id SERIAL NOT NULL,
	class_name VARCHAR(9)
);
ALTER TABLE classes ADD CONSTRAINT classes_primary_key PRIMARY KEY (classes_id);
ALTER TABLE classes ADD CONSTRAINT classes_unique UNIQUE (classes_id);

CREATE TABLE students (
	student_id SERIAL NOT NULL,
	personal_id INT,
	address_id INT,
	class_id INT,
	add_date DATE NOT NULL DEFAULT NOW()
);
ALTER TABLE students ADD CONSTRAINT students_id_pk PRIMARY KEY (student_id);
ALTER TABLE students ADD CONSTRAINT students_unique UNIQUE (student_id);
ALTER TABLE students ADD CONSTRAINT students_personal_fk FOREIGN KEY (personal_id) REFERENCES personal_data(person_id) MATCH FULL;
ALTER TABLE students ADD CONSTRAINT students_address_fk FOREIGN KEY (address_id) REFERENCES address(address_id);

CREATE TABLE users_groups (
	group_id SERIAL,
	group_name VARCHAR(10) NOT NULL,
	level_access BINARY NOT NULL DEFAULT 0001
);
ALTER TABLE users_groups ADD CONSTRAINT users_groups_pk PRIMARY KEY (group_id, group_name);
ALTER TABLE users_groups ADD CONSTRAINT users_groups_unique UNIQUE (group_id, group_name);

CREATE TABLE users (
	user_id SERIAL,
	username VARCHAR(10) NOT NULL,
	password VARCHAR(100) NOT NULL,
	name VARCHAR(50) NOT NULL,
	lastname VARCHAR(50) NOT NULL,
	email VARCHAR(60) NOT NULL
); 
ALTER TABLE users ADD CONSTRAINT users_id_pk PRIMARY KEY (user_id, email);
ALTER TABLE users ADD CONSTRAINT users_unique UNIQUE (user_id, email);

CREATE TABLE notes (
	note_id SERIAL PRIMARY KEY NOT NULL,
	user_id INT REFERENCES users(user_id),
	student_id INT REFERENCES students(student_id),
	note TEXT not null,
	add_date DATE NOT NULL DEFAULT NOW()
);